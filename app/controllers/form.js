import Controller from '@ember/controller';
import {action} from '@ember/object';
import {tracked} from '@glimmer/tracking';

export default class FormController extends Controller {
    @tracked directionArray=["up","down","right","left"]
    @tracked direction="up";
    @tracked bool=false;
    @tracked email;
    @tracked name;
    @action
    watchmail(event){
        this.email=event.target.value
    }
    @action
    watchname(event){
    this.name=event.target.value
    }
    @action 
    show(){
        if(this.name &&this.email!=" "){
            this.bool=true
            console.log (this.name+"   "+this.email);
        }
        else{
            alert("insufficient data")
       }   
    }
    @action
    change(){
        this.direction=this.directionArray[Math.floor(Math.random()*this.directionArray.length)];
    }



}
